package com.samurainex;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class Request {

    private String urlString;

    private String content;

    private int timeout;

    public Request(String urlString, int timeout) {
        //installTrustAllManager();
        this.urlString = urlString;
        this.timeout = timeout * 1000;
    }

    public URLConnection getUrlConnection() throws IOException {
        URL url = new URL(toString());
        System.out.println(url.toString());
        URLConnection urlConnection = (URLConnection) url.openConnection();
        urlConnection.setReadTimeout(timeout);
        urlConnection.setConnectTimeout(timeout);
        return urlConnection;
    }

    public String createRequest() throws IOException {
        URLConnection conn = getUrlConnection();
        if (conn instanceof HttpsURLConnection) {
            System.out.println("This connection is using HTTPS");
        }

        // Get the response
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn
                .getInputStream()));
        StringBuffer result = new StringBuffer();
        try {
            String line;
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
        } catch (IOException e) {
            throw e;
        } finally {
            if (rd != null) {
                rd.close();
            }
        }
        return result.toString();
    }

    public String toString() {
        return urlString + "?" + content;
    }

    public static void installTrustAllManager() {
        try {
            TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return null;
                        }

                        public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                        }

                        public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                        }
                    }
            };

            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(
                    new HostnameVerifier() {
                        public boolean verify(String urlHostName, SSLSession session) {
                            return true;
                        }
                    }
            );
        } catch (java.security.GeneralSecurityException e) {
        }
    }
}
